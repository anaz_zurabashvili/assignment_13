package com.example.assignment_13.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Statement(
    val id: Int?,
    val title: String?,
    @Json(name = "img-url")
    val imgUrl1: String?,
    @Json(name = "img_url")
    val imgUrl2: String?,
    val description: String?,
    val date: String?,
    val images: String?,
    @Json(name = "created_at")
    val createdAt: String?,
    @Json(name = "deleted_at")
    val deletedAt: String?,
    @Json(name = "updated_at")
    val updatedAt: String?,
){
    val imgUrl = imgUrl1 ?: imgUrl2
}
