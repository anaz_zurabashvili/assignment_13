package com.example.assignment_13.view_models

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.assignment_13.models.Resource
import com.example.assignment_13.models.Statement
import com.example.assignment_13.networking.NetworkClient
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class StatementViewModel : ViewModel() {

    private var _newsResponse = MutableLiveData<Resource<List<Statement>>>()
    val newsResponse: LiveData<Resource<List<Statement>>>
        get() = _newsResponse

    fun load() {
        viewModelScope.launch {
            withContext(IO) {
                getStatement()
            }
        }
    }

    private suspend fun getStatement() {
        _newsResponse.postValue(Resource.Loading())
        try {
            val result = NetworkClient.apiService.getStatements()
            val body = result.body()
            if (result.isSuccessful && body != null) {
                _newsResponse.postValue(Resource.Success(body))
            } else {
                _newsResponse.postValue(Resource.Error(data = null, result.message().toString()))
            }
        } catch (e: Exception) {
            _newsResponse.postValue(Resource.Error(data = null, e.toString()))
        }
    }

}