package com.example.assignment_13.networking

import com.example.assignment_13.models.Statement
import com.example.assignment_13.utils.Constants.Companion.GET_STATEMENTS
import retrofit2.Response
import retrofit2.http.GET


interface ApiService {

    @GET(GET_STATEMENTS)
    suspend fun getStatements(): Response<List<Statement>>

}