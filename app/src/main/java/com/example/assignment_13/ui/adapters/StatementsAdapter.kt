package com.example.assignment_13.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.assignment_13.databinding.ItemStatementBinding
import com.example.assignment_13.models.Statement
import com.example.assignment_13.utils.setImageUrl

class StatementsAdapter: RecyclerView.Adapter<StatementsAdapter.ViewHolder>() {

    private var news = listOf<Statement>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            ItemStatementBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(
        holder: StatementsAdapter.ViewHolder,
        position: Int
    ) =
        holder.onBind()

    inner class ViewHolder(private val binding: ItemStatementBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var model: Statement
        fun onBind() {
            model = news[adapterPosition]
            binding.tvTitle.text = model.title
            binding.tvDescription.text = model.description
            binding.tvPublishDate.text = model.date
            binding.imCover.setImageUrl(model.imgUrl)
        }
    }
    override fun getItemCount() = news.size

    fun setData(news: List<Statement>) {
        this.news = news
        notifyDataSetChanged()
    }
}