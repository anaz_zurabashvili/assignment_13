package com.example.assignment_13.ui.fragments

import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.assignment_13.abstracts.BaseFragment
import com.example.assignment_13.databinding.FragmentStatementsBinding
import com.example.assignment_13.models.Resource
import com.example.assignment_13.ui.adapters.StatementsAdapter
import com.example.assignment_13.utils.*
import com.example.assignment_13.view_models.StatementViewModel

class StatementsFragment :
    BaseFragment<FragmentStatementsBinding>(FragmentStatementsBinding::inflate) {

    private lateinit var newsAdapter: StatementsAdapter
    private val viewModel: StatementViewModel by viewModels()

    override fun init() {
        initRV()
        listeners()
        observes()
        viewModel.load()
    }

    private fun listeners() {
        binding.swipeStatementFragment.setOnRefreshListener {
            viewModel.load()
        }
    }

    private fun initRV() =
        binding.rvStatements.apply {
            newsAdapter = StatementsAdapter()
            adapter = newsAdapter
            layoutManager = LinearLayoutManager(view?.context)
        }

    private fun observes() {
        viewModel.newsResponse.observe(viewLifecycleOwner, {
            when (it) {
                is Resource.Loading -> {
                    binding.swipeStatementFragment.refreshing(true)
                }
                is Resource.Success -> {
                    binding.swipeStatementFragment.refreshing(false)
                    newsAdapter.setData(it.data!!)
                }
                is Resource.Error -> {
                    binding.swipeStatementFragment.refreshing(true)
                }
            }
        })
    }
}