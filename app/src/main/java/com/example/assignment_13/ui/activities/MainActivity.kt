package com.example.assignment_13.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.assignment_13.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}